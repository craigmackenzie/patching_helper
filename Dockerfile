FROM 861698843696.dkr.ecr.eu-west-1.amazonaws.com/build-aws:latest

COPY . /

WORKDIR /

USER root

RUN pip3 install --no-cache-dir -r requirements.txt

USER jenkins