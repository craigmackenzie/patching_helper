from tabulate import tabulate
from datetime import datetime
from utils import fetch_parameters, return_ami_id, return_dsap
import boto3


paths = {
    'beehive_ami': '/beehive/ami/id',
    'rundeck_ami': '/rundeck/ami/id',
    'golden_ecs_ami': '/GoldenAMI/PPL-AmazonLinux2/ECS/latest',
    'cli_tools_ami': '/beeh-cli-tools/ami/id',
    'golden_standard_ami': '/GoldenAMI/PPL-AmazonLinux2/Standard/latest'
}

dsap = return_dsap()
all_params = fetch_parameters(paths)

params = {i["Name"]: [i["Value"], i["LastModifiedDate"].strftime('%Y-%m-%d %H:%M:%S')] for i in [j for j in all_params]}

beehive_deployed_ami = return_ami_id('Beehive ECS Container Instance')
rundeck_deployed_ami = return_ami_id('rundeck*')
tooling_deployed_ami = return_ami_id('beehive-activemq-cli*')

beehive_row = [
        'Beehive (ecs)', 
        beehive_deployed_ami,
        params[paths['beehive_ami']][0], 
        params[paths['golden_ecs_ami']][0], 
        params[paths['golden_ecs_ami']][1]
    ]

rundeck_row = [
        'Rundeck (ecs)',
        rundeck_deployed_ami,
        params[paths['rundeck_ami']][0], 
        params[paths['golden_ecs_ami']][0], 
        params[paths['golden_ecs_ami']][1]
    ]

tooling_row = [
        'CLI tools',
        tooling_deployed_ami,
        params[paths['cli_tools_ami']][0], 
        params[paths['golden_standard_ami']][0], 
        params[paths['golden_standard_ami']][1]
    ]

table_headers = ['Service', 'Current Deployed AMI', 'Current Set AMI ID', 'Golden AMI', 'Golden AMI Created Date']
table = [beehive_row, rundeck_row, tooling_row]

print(' ')
print(' ')
print(f"AMI values for: {dsap}")
print(tabulate(table, headers=table_headers))
print(' ')
print(' ')