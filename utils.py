import boto3

region = 'eu-west-1'

ec2_client = boto3.client('ec2', region_name=region)
ssm_client = boto3.client('ssm', region_name=region)
sts_client = boto3.client('sts', region_name=region)

DSAPS = {
  '854680800964': 'Development',
  '107538011926': 'Staging',
  '113268170478': 'Acceptance',
  '163568250066': 'PRODUCTION',
}

def return_ami_id(filter):
    response = ec2_client.describe_instances(
        Filters=[
            {'Name': 'tag:Name', 'Values': [filter]},
            {'Name': 'instance-state-code', 'Values': ['16']},
        ]
    )

    return response['Reservations'][0]['Instances'][0]['ImageId']


def fetch_parameters(params):
    response = ssm_client.get_parameters(
        Names=list(params.values()),
        WithDecryption=True
    )
    return response.get("Parameters")


def return_dsap():
  response = sts_client.get_caller_identity()
  return DSAPS[response["Account"]]